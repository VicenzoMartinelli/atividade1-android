package br.edu.utfpr.br.atividade1.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GeoResult {

  @SerializedName("address_components")
  @Expose
  private List<AddressComponent> addressComponents = null;
  @SerializedName("formatted_address")
  @Expose
  private String formattedAddress;
  @SerializedName("geometry")
  @Expose
  private Geometry geometry;
  @SerializedName("place_id")
  @Expose
  private String placeId;
  @SerializedName("plus_code")
  @Expose
  private PlusCode_ plusCode;
  @SerializedName("types")
  @Expose
  private List<String> types = null;

  /**
   * No args constructor for use in serialization
   *
   */
  public GeoResult() {
  }

  /**
   *
   * @param placeId
   * @param plusCode
   * @param formattedAddress
   * @param types
   * @param addressComponents
   * @param geometry
   */
  public GeoResult(List<AddressComponent> addressComponents, String formattedAddress, Geometry geometry, String placeId, PlusCode_ plusCode, List<String> types) {
    super();
    this.addressComponents = addressComponents;
    this.formattedAddress = formattedAddress;
    this.geometry = geometry;
    this.placeId = placeId;
    this.plusCode = plusCode;
    this.types = types;
  }

  public List<AddressComponent> getAddressComponents() {
    return addressComponents;
  }

  public void setAddressComponents(List<AddressComponent> addressComponents) {
    this.addressComponents = addressComponents;
  }

  public String getFormattedAddress() {
    return formattedAddress;
  }

  public void setFormattedAddress(String formattedAddress) {
    this.formattedAddress = formattedAddress;
  }

  public Geometry getGeometry() {
    return geometry;
  }

  public void setGeometry(Geometry geometry) {
    this.geometry = geometry;
  }

  public String getPlaceId() {
    return placeId;
  }

  public void setPlaceId(String placeId) {
    this.placeId = placeId;
  }

  public PlusCode_ getPlusCode() {
    return plusCode;
  }

  public void setPlusCode(PlusCode_ plusCode) {
    this.plusCode = plusCode;
  }

  public List<String> getTypes() {
    return types;
  }

  public void setTypes(List<String> types) {
    this.types = types;
  }
}
