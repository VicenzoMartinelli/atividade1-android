package br.edu.utfpr.br.atividade1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.card.MaterialCardView;

import java.util.List;

import br.com.rafael.jpdroid.core.Jpdroid;
import br.edu.utfpr.br.atividade1.model.Ponto;

public class PontoAdapter extends BaseAdapter
{

  private Context context;
  private Cursor cursor;
  private Jpdroid bd;
  private LayoutInflater inflater;

  public PontoAdapter(Context context, Cursor cursor, Jpdroid bd)
  {
    this.context = context;
    this.cursor = cursor;
    this.bd = bd;

    inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount()
  {
    return cursor.getCount();
  }

  @Override
  public Object getItem(int i)
  {
    return cursor.moveToPosition(i);
  }

  @Override
  public long getItemId(int i)
  {
    cursor.moveToPosition(i);
    return cursor.getLong(cursor.getColumnIndex("_id"));
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup)
  {
    View v = inflater.inflate(R.layout.lista_item, null);

    final TextView   tvDescricao = v.findViewById(R.id.tvDescricao);
    TextView         tvEndereco  = v.findViewById(R.id.tvEndereco);
    ImageView        ivFoto      = v.findViewById(R.id.ivFoto);

    List<Ponto> lista = bd.retrieve(Ponto.class);
    //Esta trazendo 0 resultados na lista e ta dando erro
    final Ponto cad = lista.get(i);

    tvDescricao.setText(String.valueOf(cad.getDescricao()));
    tvEndereco.setText(cad.getEndereco());
    ivFoto.setImageBitmap(cad.getFoto());

    return v;
  }
}
