package br.edu.utfpr.br.atividade1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import br.com.rafael.jpdroid.core.Jpdroid;
import br.edu.utfpr.br.atividade1.model.Ponto;

public class ListaPontoActivity extends AppCompatActivity
{

  private Jpdroid _bd;
  private ListView lista;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lista_ponto);

    lista = findViewById(R.id.lvLista);

    _bd = Jpdroid.getInstance();
    _bd.setDatabaseName("pontost");
    _bd.setContext(this);
    _bd.setDatabaseVersion(5);
    _bd.addEntity(Ponto.class);
    _bd.open();

    Cursor registros = _bd.query("Ponto", null, null, null, null, null, null);

    final PontoAdapter adapter = new PontoAdapter(this, registros, _bd);

    lista.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id)
      {
        long idAlter = adapter.getItemId(position);

        editarPonto(view, idAlter);
      }
    });

    lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
    {
      @Override
      public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
      {
        final int pos = position;
        final AdapterView<?> par = parent;

        AlertDialog.Builder alerta = new AlertDialog.Builder(ListaPontoActivity.this);
        alerta.setTitle("Atenção");
        alerta.setMessage("Deseja realmente excluir seu ponto salvo?");
        alerta.setCancelable(true);
        alerta.setPositiveButton("Sim", new DialogInterface.OnClickListener()
        {
          @Override
          public void onClick(DialogInterface dialog, int which)
          {
            long idDelete = adapter.getItemId(pos);
            if(deleteItem(par, adapter, pos, idDelete))
            {
              PontoAdapter adapter = new PontoAdapter(
                  ListaPontoActivity.this,
                  _bd.query("Ponto", null, null, null, null, null, null),
                  _bd);

              Toast.makeText(ListaPontoActivity.this, "Registro excluído com sucesso!!!", Toast.LENGTH_SHORT).show();

              lista.setAdapter(adapter);
            }
          }
        });
        alerta.setNegativeButton("Não", new DialogInterface.OnClickListener()
        {
          @Override
          public void onClick(DialogInterface dialog, int which)
          {
            dialog.cancel();
          }
        });
        alerta.show();

        return true;
      }
    });

    lista.setAdapter(adapter);
  }

  private boolean deleteItem(AdapterView<?> parent, PontoAdapter adapter,  int position, long idDelete)
  {
    try
    {
      final PontoAdapter ps = adapter;
      final int pos = position;
      Ponto p = Ponto.obterItemDeLista(_bd.retrieve(Ponto.class), idDelete);

      _bd.delete(p);

      return true;

    } catch (Exception e)
    {
      e.printStackTrace();
      Toast.makeText(this, "Falhou", Toast.LENGTH_SHORT).show();
      return false;
    }
  }

  private void editarPonto(View view, long id)
  {
    Intent i = new Intent(this, CadastroPontoActivity.class);
    i.putExtra("id", id);
    i.putExtra("viewId", view.getId());
    this.startActivityForResult(i, 666);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
  {
    if (requestCode == 666 && resultCode == 0 && data != null)
    {
      View v = findViewById(data.getIntExtra("viewId", 0));

      Ponto p = (Ponto) data.getSerializableExtra("obj");

      ImageView im          = v.findViewById(R.id.ivFoto);
      TextView  tvDescricao = v.findViewById(R.id.tvDescricao);
      TextView  tvEndereco  = v.findViewById(R.id.tvEndereco);

      im.setImageBitmap(p.getFoto());
      tvDescricao.setText(p.getDescricao());
      tvEndereco.setText(p.getEndereco());
    }
  }
}
