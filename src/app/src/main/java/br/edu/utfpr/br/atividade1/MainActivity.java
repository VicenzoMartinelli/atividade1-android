package br.edu.utfpr.br.atividade1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import br.com.rafael.jpdroid.core.Jpdroid;
import br.edu.utfpr.br.atividade1.model.Ponto;

public class MainActivity extends AppCompatActivity
{
  private Jpdroid _bd;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    _bd = Jpdroid.getInstance();
    _bd.setDatabaseName("pontost");
    _bd.setContext(this);
    _bd.setDatabaseVersion(5);
    _bd.addEntity(Ponto.class);
    _bd.open();
  }

  public void btIncluirOnclick(View view)
  {
    startActivity(new Intent(this, CadastroPontoActivity.class));
  }

  public void btListarOnClick(View view)
  {
    startActivity(new Intent(this, ListaPontoActivity.class));
  }

  public void btVerMapaOnClick(View view) {
    startActivity(new Intent(this, MapActivity.class));
  }
}
