package br.edu.utfpr.br.atividade1;

import androidx.fragment.app.FragmentActivity;
import br.com.rafael.jpdroid.core.Jpdroid;
import br.edu.utfpr.br.atividade1.model.Ponto;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback
{

  private GoogleMap mMap;
  private Jpdroid _bd;


  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_map);

    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
  }

  @Override
  public void onMapReady(GoogleMap googleMap)
  {
    mMap = googleMap;

    _bd = Jpdroid.getInstance();
    _bd.setDatabaseName("pontost");
    _bd.setContext(this);
    _bd.setDatabaseVersion(5);
    _bd.addEntity(Ponto.class);
    _bd.open();

    LatLng posInitial = null;
    for(Ponto p : _bd.retrieve(Ponto.class))
    {
      if(p.getLatitude() != 0.0 && p.getLongitude() != 0.0)
      {
        LatLng pos = new LatLng(p.getLatitude(), p.getLongitude());
        mMap.addMarker(new MarkerOptions().position(pos).title(p.getoQueSignificaPraMim()));

        if(posInitial == null)
          posInitial = pos;
      }
    }

    if(posInitial != null)
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(posInitial, 20));
  }
}
