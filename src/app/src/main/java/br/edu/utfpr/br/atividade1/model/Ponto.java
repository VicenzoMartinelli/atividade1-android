package br.edu.utfpr.br.atividade1.model;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.List;

import br.com.rafael.jpdroid.annotations.Column;
import br.com.rafael.jpdroid.annotations.Entity;
import br.com.rafael.jpdroid.annotations.PrimaryKey;

@Entity
public class Ponto implements Serializable {

  @Column
  @PrimaryKey
  private Long _id;

  @Column
  private Bitmap foto;
  @Column
  private String descricao;
  @Column
  private String endereco;
  @Column
  private String bairro;
  @Column
  private String numero;
  @Column
  private String cep;
  @Column
  private String cidade;
  @Column
  private String oQueSignificaPraMim;
  @Column
  private double latitude;
  @Column
  private double longitude;

  public Long get_id()
  {
    return _id;
  }

  public void set_id(Long _id)
  {
    this._id = _id;
  }

  public String getEndereco()
  {
    return endereco;
  }

  public void setEndereco(String endereco)
  {
    this.endereco = endereco;
  }

  public String getBairro()
  {
    return bairro;
  }

  public void setBairro(String bairro)
  {
    this.bairro = bairro;
  }

  public String getNumero()
  {
    return numero;
  }

  public void setNumero(String numero)
  {
    this.numero = numero;
  }

  public String getCep()
  {
    return cep;
  }

  public void setCep(String cep)
  {
    this.cep = cep;
  }

  public String getDescricao()
  {
    return descricao;
  }

  public void setDescricao(String descricao)
  {
    this.descricao = descricao;
  }

  public String getoQueSignificaPraMim()
  {
    return oQueSignificaPraMim;
  }

  public void setoQueSignificaPraMim(String oQueSignificaPraMim)
  {
    this.oQueSignificaPraMim = oQueSignificaPraMim;
  }

  public double getLatitude()
  {
    return latitude;
  }

  public void setLatitude(double latitude)
  {
    this.latitude = latitude;
  }

  public double getLongitude()
  {
    return longitude;
  }

  public void setLongitude(double longitude)
  {
    this.longitude = longitude;
  }

  public Bitmap getFoto()
  {
    return foto;
  }

  public void setFoto(Bitmap foto)
  {
    this.foto = foto;
  }

  public String getCidade()
  {
    return cidade;
  }

  public void setCidade(String cidade)
  {
    this.cidade = cidade;
  }

  public Ponto()
  {
  }

  public Ponto(Long _id, Bitmap foto, String descricao, String endereco, String bairro, String numero, String cep, String cidade, String oQueSignificaPraMim, double latitude, double longitude)
  {
    this._id = _id;
    this.foto = foto;
    this.descricao = descricao;
    this.endereco = endereco;
    this.bairro = bairro;
    this.numero = numero;
    this.cep = cep;
    this.cidade = cidade;
    this.oQueSignificaPraMim = oQueSignificaPraMim;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public Ponto(Bitmap foto, String descricao, String endereco, String bairro, String numero, String cep, String cidade, String oQueSignificaPraMim, double latitude, double longitude)
  {
    this.foto = foto;
    this.descricao = descricao;
    this.endereco = endereco;
    this.bairro = bairro;
    this.numero = numero;
    this.cep = cep;
    this.cidade = cidade;
    this.oQueSignificaPraMim = oQueSignificaPraMim;
    this.latitude = latitude;
    this.longitude = longitude;
  }

  public static Ponto obterItemDeLista(List<Ponto> retrieve, long id)
  {
    for(Ponto point : retrieve)
    {
      if(point.get_id() == id)
        return point;
    }

    return null;
  }
}
