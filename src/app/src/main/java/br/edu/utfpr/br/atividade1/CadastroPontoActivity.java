package br.edu.utfpr.br.atividade1;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import br.com.rafael.jpdroid.core.Jpdroid;
import br.com.rafael.jpdroid.exceptions.JpdroidException;
import br.edu.utfpr.br.atividade1.model.AddressComponent;
import br.edu.utfpr.br.atividade1.model.GeoResult;
import br.edu.utfpr.br.atividade1.model.GeocodeModel;
import br.edu.utfpr.br.atividade1.model.Ponto;

public class CadastroPontoActivity extends AppCompatActivity implements LocationListener
{
  private Jpdroid _bd;
  private LocationManager _lm;
  private Ponto pt;
  private TextInputEditText etCep;
  private TextInputEditText etEndereco;
  private TextInputEditText etNumero;
  private TextInputEditText etBairro;
  private TextInputEditText etDescricao;
  private TextInputEditText etCidade;
  private TextInputEditText etSignifica;
  private ImageView ivFoto;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cadastro_ponto);

    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

    StrictMode.setThreadPolicy(policy);

    _lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

    if (!_lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
    {
      AlertDialog.Builder alerta = new AlertDialog.Builder(this);
      alerta.setTitle("Atenção");
      alerta.setMessage("GPS não habilitado. Deseja Habilitar?");
      alerta.setCancelable(true);
      alerta.setPositiveButton("OK", new DialogInterface.OnClickListener()
      {
        @Override
        public void onClick(DialogInterface dialog, int which)
        {
          Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
          startActivity(i);
        }
      });
      alerta.show();
    }

    _bd = Jpdroid.getInstance();
    _bd.setDatabaseName("pontost");
    _bd.setContext(this);
    _bd.setDatabaseVersion(5);
    _bd.addEntity(Ponto.class);
    _bd.open();

    etCep = findViewById(R.id.etCep);
    etCidade = findViewById(R.id.etCidade);
    etBairro = findViewById(R.id.etBairro);
    etDescricao = findViewById(R.id.etDescricao);
    etEndereco = findViewById(R.id.etEndereco);
    etNumero = findViewById(R.id.etNumero);
    etSignifica = findViewById(R.id.etSignifica);
    ivFoto = findViewById(R.id.ivFoto);

    final long idAlterar = getIntent().getLongExtra("id", -99999);

    if(idAlterar != -99999)
    {
      pt = obterItemDeLista(_bd.retrieve(Ponto.class), idAlterar);

      etCep.setText(pt.getCep());
      etBairro.setText(pt.getBairro());
      etCidade.setText(pt.getCidade());
      etDescricao.setText(pt.getDescricao());
      etNumero.setText(pt.getNumero());
      etSignifica.setText(pt.getoQueSignificaPraMim());
      etEndereco.setText(pt.getEndereco());
      ivFoto.setImageBitmap(pt.getFoto());
    }
    else{
      pt = new Ponto();
    }

    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)
    {
      requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET, Manifest.permission.ACCESS_COARSE_LOCATION}, 500);
    }
    _lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
  }

  private Ponto obterItemDeLista(List<Ponto> retrieve, long idAlterar)
  {
    for(Ponto point : retrieve)
    {
      if(point.get_id() == idAlterar)
        return point;
    }

    return null;
  }

  @Override
  public void onLocationChanged(Location location)
  {
  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras)
  {

  }

  @Override
  public void onProviderEnabled(String provider)
  {

  }

  @Override
  public void onProviderDisabled(String provider)
  {

  }

  public void btnAtualizarLocalizacaoClick(View view)
  {
    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
    {
      return;
    }
    Location loc = _lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

    if (loc != null)
    {
      String key = "AIzaSyDyscwVqwGVLuqB0Gqs4pZBXTpfp9pigfk";
      String urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + loc.getLatitude() + "," + loc.getLongitude() + "&key=" + key;

      URL url = null;
      try
      {
        url = new URL(urlString);

        URLConnection conn = url.openConnection();

        String cont = convertStreamToString(conn.getInputStream());

        GeocodeModel r = new GsonBuilder().create().fromJson(cont, GeocodeModel.class);

        GeoResult x = r.getResults().get(0);

        if (x == null)
          return;

        this.pt.setLongitude(loc.getLongitude());
        this.pt.setLatitude(loc.getLatitude());

        for (AddressComponent comp : x.getAddressComponents())
        {
          String type = getComponentAddressType(comp);

          switch (type)
          {
            case "CEP":
              etCep.setText(comp.getLongName());
              break;
            case "CIDADE":
              etCidade.setText(comp.getLongName());
              break;
            case "ENDERECO":
              etEndereco.setText(comp.getLongName());
              break;
            case "BAIRRO":
              etBairro.setText(comp.getLongName());
              break;
            case "NUMERO":
              etNumero.setText(comp.getLongName());
              break;
          }
        }

      } catch (Exception e)
      {
        e.printStackTrace();
      }
    }
  }

  private String getComponentAddressType(AddressComponent comp)
  {
    for (String t : comp.getTypes())
    {
      switch (t)
      {
        case "street_number":
          return "NUMERO";
        case "route":
          return "ENDERECO";
        case "sublocality_level_1":
          return "BAIRRO";
        case "administrative_area_level_2":
          return "CIDADE";
        case "postal_code":
          return "CEP";
      }
    }
    return "";
  }

  private static String convertStreamToString(InputStream is)
  {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder  sb     = new StringBuilder();

    String line = null;
    try
    {
      while ((line = reader.readLine()) != null)
      {
        sb.append(line + "\n");
      }
    } catch (IOException e)
    {
      e.printStackTrace();
    } finally
    {
      try
      {
        is.close();
      } catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    return sb.toString();
  }

  public void btSalvarOnClick(View view)
  {
    this.pt.setDescricao(this.etDescricao.getText().toString());
    this.pt.setEndereco(this.etEndereco.getText().toString());
    this.pt.setCep(this.etCep.getText().toString());
    this.pt.setNumero(this.etNumero.getText().toString());
    this.pt.setBairro(this.etBairro.getText().toString());
    this.pt.setCidade(this.etCidade.getText().toString());
    this.pt.setoQueSignificaPraMim(this.etSignifica.getText().toString());

    try
    {
      _bd.persist(this.pt);
      Toast.makeText(this, "Ponto turístico salvo com sucesso!", Toast.LENGTH_SHORT).show();

      Intent i = new Intent();

      i.putExtra("obj", this.pt);
      i.putExtra("viewId", getIntent().getIntExtra("viewId", 0));
      setResult(0, i);

      finish();
    } catch (JpdroidException e)
    {
      e.printStackTrace();
    }

  }

  private void limparCampos()
  {
    etCep.setText("");
    etEndereco.setText("");
    etNumero.setText("");
    etBairro.setText("");
    etDescricao.setText("");
    etCidade.setText("");
    etSignifica.setText("");
    ivFoto.setImageBitmap(null);
  }

  public void btAttFotoClick(View view)
  {
    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

    startActivityForResult(i, 888);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
  {
    if (requestCode == 888 && resultCode == RESULT_OK)
    {
      Bitmap fot = (Bitmap) data.getExtras().get("data");
      if (fot != null)
        pt.setFoto(fot);

      ivFoto.setImageBitmap(fot);
    }
  }
}
