package br.edu.utfpr.br.atividade1.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


class Bounds {

  @SerializedName("northeast")
  @Expose
  private Northeast_ northeast;
  @SerializedName("southwest")
  @Expose
  private Southwest_ southwest;

  /**
   * No args constructor for use in serialization
   *
   */
  public Bounds() {
  }

  /**
   *
   * @param southwest
   * @param northeast
   */
  public Bounds(Northeast_ northeast, Southwest_ southwest) {
    super();
    this.northeast = northeast;
    this.southwest = southwest;
  }

  public Northeast_ getNortheast() {
    return northeast;
  }

  public void setNortheast(Northeast_ northeast) {
    this.northeast = northeast;
  }

  public Southwest_ getSouthwest() {
    return southwest;
  }

  public void setSouthwest(Southwest_ southwest) {
    this.southwest = southwest;
  }
}
public class GeocodeModel {

  @SerializedName("plus_code")
  @Expose
  private PlusCode plusCode;
  @SerializedName("results")
  @Expose
  private List<GeoResult> results = null;
  @SerializedName("status")
  @Expose
  private String status;

  /**
   * No args constructor for use in serialization
   *
   */
  public GeocodeModel() {
  }

  /**
   *
   * @param results
   * @param status
   * @param plusCode
   */
  public GeocodeModel(PlusCode plusCode, List<GeoResult> results, String status) {
    super();
    this.plusCode = plusCode;
    this.results = results;
    this.status = status;
  }

  public PlusCode getPlusCode() {
    return plusCode;
  }

  public void setPlusCode(PlusCode plusCode) {
    this.plusCode = plusCode;
  }

  public List<GeoResult> getResults() {
    return results;
  }

  public void setResults(List<GeoResult> results) {
    this.results = results;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}

class Geometry {

  @SerializedName("location")
  @Expose
  private Location location;
  @SerializedName("location_type")
  @Expose
  private String locationType;
  @SerializedName("viewport")
  @Expose
  private Viewport viewport;
  @SerializedName("bounds")
  @Expose
  private Bounds bounds;

  /**
   * No args constructor for use in serialization
   *
   */
  public Geometry() {
  }

  /**
   *
   * @param bounds
   * @param viewport
   * @param location
   * @param locationType
   */
  public Geometry(Location location, String locationType, Viewport viewport, Bounds bounds) {
    super();
    this.location = location;
    this.locationType = locationType;
    this.viewport = viewport;
    this.bounds = bounds;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public String getLocationType() {
    return locationType;
  }

  public void setLocationType(String locationType) {
    this.locationType = locationType;
  }

  public Viewport getViewport() {
    return viewport;
  }

  public void setViewport(Viewport viewport) {
    this.viewport = viewport;
  }

  public Bounds getBounds() {
    return bounds;
  }

  public void setBounds(Bounds bounds) {
    this.bounds = bounds;
  }
}
class Location {

  @SerializedName("lat")
  @Expose
  private double lat;
  @SerializedName("lng")
  @Expose
  private double lng;

  /**
   * No args constructor for use in serialization
   *
   */
  public Location() {
  }

  /**
   *
   * @param lng
   * @param lat
   */
  public Location(double lat, double lng) {
    super();
    this.lat = lat;
    this.lng = lng;
  }

  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double getLng() {
    return lng;
  }

  public void setLng(double lng) {
    this.lng = lng;
  }

}

class Northeast {

  @SerializedName("lat")
  @Expose
  private double lat;
  @SerializedName("lng")
  @Expose
  private double lng;

  /**
   * No args constructor for use in serialization
   *
   */
  public Northeast() {
  }

  /**
   *
   * @param lng
   * @param lat
   */
  public Northeast(double lat, double lng) {
    super();
    this.lat = lat;
    this.lng = lng;
  }

  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double getLng() {
    return lng;
  }

  public void setLng(double lng) {
    this.lng = lng;
  }
}

class Northeast_ {

  @SerializedName("lat")
  @Expose
  private double lat;
  @SerializedName("lng")
  @Expose
  private double lng;

  /**
   * No args constructor for use in serialization
   *
   */
  public Northeast_() {
  }

  /**
   *
   * @param lng
   * @param lat
   */
  public Northeast_(double lat, double lng) {
    super();
    this.lat = lat;
    this.lng = lng;
  }

  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double getLng() {
    return lng;
  }

  public void setLng(double lng) {
    this.lng = lng;
  }
}

class PlusCode {

  @SerializedName("compound_code")
  @Expose
  private String compoundCode;
  @SerializedName("global_code")
  @Expose
  private String globalCode;

  /**
   * No args constructor for use in serialization
   *
   */
  public PlusCode() {
  }

  /**
   *
   * @param compoundCode
   * @param globalCode
   */
  public PlusCode(String compoundCode, String globalCode) {
    super();
    this.compoundCode = compoundCode;
    this.globalCode = globalCode;
  }

  public String getCompoundCode() {
    return compoundCode;
  }

  public void setCompoundCode(String compoundCode) {
    this.compoundCode = compoundCode;
  }

  public String getGlobalCode() {
    return globalCode;
  }

  public void setGlobalCode(String globalCode) {
    this.globalCode = globalCode;
  }

}
class PlusCode_ {

  @SerializedName("compound_code")
  @Expose
  private String compoundCode;
  @SerializedName("global_code")
  @Expose
  private String globalCode;

  /**
   * No args constructor for use in serialization
   *
   */
  public PlusCode_() {
  }

  /**
   *
   * @param compoundCode
   * @param globalCode
   */
  public PlusCode_(String compoundCode, String globalCode) {
    super();
    this.compoundCode = compoundCode;
    this.globalCode = globalCode;
  }

  public String getCompoundCode() {
    return compoundCode;
  }

  public void setCompoundCode(String compoundCode) {
    this.compoundCode = compoundCode;
  }

  public String getGlobalCode() {
    return globalCode;
  }

  public void setGlobalCode(String globalCode) {
    this.globalCode = globalCode;
  }
}

class Southwest {

  @SerializedName("lat")
  @Expose
  private double lat;
  @SerializedName("lng")
  @Expose
  private double lng;

  /**
   * No args constructor for use in serialization
   *
   */
  public Southwest() {
  }

  /**
   *
   * @param lng
   * @param lat
   */
  public Southwest(double lat, double lng) {
    super();
    this.lat = lat;
    this.lng = lng;
  }

  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double getLng() {
    return lng;
  }

  public void setLng(double lng) {
    this.lng = lng;
  }
}
class Southwest_ {

  @SerializedName("lat")
  @Expose
  private double lat;
  @SerializedName("lng")
  @Expose
  private double lng;

  /**
   * No args constructor for use in serialization
   *
   */
  public Southwest_() {
  }

  /**
   *
   * @param lng
   * @param lat
   */
  public Southwest_(double lat, double lng) {
    super();
    this.lat = lat;
    this.lng = lng;
  }

  public double getLat() {
    return lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public double getLng() {
    return lng;
  }

  public void setLng(double lng) {
    this.lng = lng;
  }

}
class Viewport {

  @SerializedName("northeast")
  @Expose
  private Northeast northeast;
  @SerializedName("southwest")
  @Expose
  private Southwest southwest;

  /**
   * No args constructor for use in serialization
   *
   */
  public Viewport() {
  }

  /**
   *
   * @param southwest
   * @param northeast
   */
  public Viewport(Northeast northeast, Southwest southwest) {
    super();
    this.northeast = northeast;
    this.southwest = southwest;
  }

  public Northeast getNortheast() {
    return northeast;
  }

  public void setNortheast(Northeast northeast) {
    this.northeast = northeast;
  }

  public Southwest getSouthwest() {
    return southwest;
  }

  public void setSouthwest(Southwest southwest) {
    this.southwest = southwest;
  }
}